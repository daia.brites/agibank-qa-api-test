const should = require("should")
const request = require("request")
const chai = require("chai")
const expect = chai.expect

const urlBase = "http://localhost:8999/users"

describe(`Editar User PJ com Sucesso - PUT ${urlBase}`, function () {

    const userJson = {
        "name": {
            "first": "Super",
            "last": "Homem"
        },
        "typePerson": "PESSOA_JURIDICA",
        "CNPJ": "45356745901234",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    const userJsonEdited = {
        "name": {
            "first": "Super",
            "last": "Sobrenome"
        },
        "typePerson": "PESSOA_JURIDICA",
        "CNPJ": "45356745901234",
        "birth": "01/01/1900",
        "address": {
            "CEP": "92200300",
            "street": "Av secundaria",
            "number": 1313,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    var userId = -1

    it('Criar novo User e armazenar novo ID gerado', function (done) {
        request.post(
            {url: urlBase, json: userJson},
            function (error, response, body) {

                expect(response.statusCode).to.equal(201)

                if (body.should.have.property('CNPJ')) {
                    expect(body.CNPJ).to.equal(userJson.CNPJ)
                }
                // armazena ID criado para o novo User
                userId = body.id
                done()
            }
        )
    })

    it('Editar novo User gerado', function (done) {
        request.put(
            {url: urlBase + `/${userId}`, json: userJsonEdited},
            function (error, response, body) {

                expect(response.statusCode).to.equal(200)

                if (body.should.have.property('CNPJ')) {
                    expect(body.CNPJ).to.equal(userJsonEdited.CNPJ)
                    expect(body.name.last).to.equal(userJsonEdited.name.last)
                    expect(body.address.CEP).to.equal(userJsonEdited.address.CEP)
                    expect(body.address.street).to.equal(userJsonEdited.address.street)
                    expect(body.address.number).to.equal(userJsonEdited.address.number)
                }
                done()
            }
        )
    })

})

function jsonParser(jsonBody) {
    // precisa converter o retorno para um objeto JSON
    let body = {}
    try {
        body = JSON.parse(jsonBody)
    } catch (err) {
        console.log(`Erro ao converter JSON Body! ${err}`)
        body = {}
    }
    return body
}
