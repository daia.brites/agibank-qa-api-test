const should = require("should")
const request = require("request")
const chai = require("chai")
const expect = chai.expect

const urlBase = "http://localhost:8999/users"

describe(`Deletar User PF com Sucesso - DELETE ${urlBase}`, function () {

    const userJson = {
        "name": {
            "first": "Clark",
            "last": "Kent"
        },
        "typePerson": "PESSOA_FISICA",
        "CPF": "12345678901",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    var userId = -1

    it('Criar novo User e armazenar novo ID gerado', function (done) {
        request.post(
            {url: urlBase, json: userJson},
            function (error, response, body) {

                expect(response.statusCode).to.equal(201)

                if (body.should.have.property('CPF')) {
                    expect(body.CPF).to.equal(userJson.CPF)
                }
                // armazena ID criado para o novo User
                userId = body.id
                done()
            }
        )
    })

    it('Deletar novo User através do ID criado', function (done) {
        request.delete(
            {url: urlBase + `/${userId}`},
            function (error, response, _body) {

                expect(response.statusCode).to.equal(200)
                done()
            }
        )
    })

    it('Deletar novamente o novo User através do ID criado', function (done) {
        request.delete(
            {url: urlBase + `/${userId}`},
            function (error, response, _body) {

                expect(response.statusCode).to.equal(404)
                done()
            }
        )
    })

})
