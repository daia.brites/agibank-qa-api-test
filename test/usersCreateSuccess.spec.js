const should = require("should")
const request = require("request")
const chai = require("chai")
const expect = chai.expect

const urlBase = "http://localhost:8999/users"

describe(`Criar novo User PJ com Sucesso - POST ${urlBase}`, function () {

    const userJson = {
        "name": {
            "first": "Super",
            "last": "Homem"
        },
        "typePerson": "PESSOA_JURIDICA",
        "CNPJ": "52345678901234",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    var userId = -1

    it('Criar novo User e armazenar novo ID gerado', function (done) {
        request.post(
            {url: urlBase, json: userJson},
            function (error, response, body) {

                expect(response.statusCode).to.equal(201)

                if (body.should.have.property('CNPJ')) {
                    expect(body.CNPJ).to.equal(userJson.CNPJ)
                }
                // armazena ID criado para o novo User
                userId = body.id
                done()
            }
        )
    })

    it('Consulta novo User criado buscando pelo novo ID', function (done) {
        request.get(
            {url: urlBase + `/${userId}`},
            function (error, response, _body) {

                expect(response.statusCode).to.equal(200)

                let body = jsonParser(_body)
                if (body.should.have.property('CNPJ')) {
                    expect(body.CNPJ).to.equal(userJson.CNPJ)
                }
                done()
            }
        )
    })

})

describe(`Criar novo User PF com Sucesso - POST ${urlBase}`, function () {

    const userJson = {
        "name": {
            "first": "Clark",
            "last": "Kent"
        },
        "typePerson": "PESSOA_FISICA",
        "CPF": "12345678901",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    var userId = -1

    it('Criar novo User e armazenar novo ID gerado', function (done) {
        request.post(
            {url: urlBase, json: userJson},
            function (error, response, body) {

                expect(response.statusCode).to.equal(201)

                if (body.should.have.property('CPF')) {
                    expect(body.CPF).to.equal(userJson.CPF)
                }
                // armazena ID criado para o novo User
                userId = body.id
                done()
            }
        )
    })

    it('Consulta novo User criado, buscando pelo novo ID', function (done) {
        request.get(
            {url: urlBase + `/${userId}`},
            function (error, response, _body) {

                expect(response.statusCode).to.equal(200)

                let body = jsonParser(_body)
                if (body.should.have.property('CPF')) {
                    expect(body.CPF).to.equal(userJson.CPF)
                }
                done()
            }
        )
    })

})


function jsonParser(jsonBody) {
    // precisa converter o retorno para um objeto JSON
    let body = {}
    try {
        body = JSON.parse(jsonBody)
    } catch (err) {
        console.log(`Erro ao converter JSON Body! ${err}`)
        body = {}
    }
    return body
}
