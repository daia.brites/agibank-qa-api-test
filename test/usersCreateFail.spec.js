const should = require("should")
const request = require("request")
const chai = require("chai")
const expect = chai.expect

const urlBase = "http://localhost:8999/users"

describe(`Criar novo User PJ SEM Sucesso - POST ${urlBase}`, function () {

    // JSON válido utilizado como base para os cenários de falha
    const userJson = {
        "name": {
            "first": "Super",
            "last": "Homem"
        },
        "typePerson": "PESSOA_JURIDICA",
        "CNPJ": "52345678901234",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    it('Criar novo User sem Last Name', function (done) {

        // Clona JSON original válido
        const userWithoutLastName = JSON.parse(JSON.stringify(userJson))
        // Remove conteúdo de 'name.last'
        userWithoutLastName.name.last = ''

        request.post(
            {url: urlBase, json: userWithoutLastName},
            function (error, response, body) {

                expect(response.statusCode).to.equal(400)

                if (body.should.have.property('errors')) {
                    expect(body.errors[0].property).to.equal('instance.name.last')
                }
                done()
            }
        )
    })

    it('Criar novo User com CNPJ inválido', function (done) {

        // Clona JSON original válido
        const userWithInvalidCNPJ = JSON.parse(JSON.stringify(userJson))
        // Altera para 'CNPJ' inválido
        userWithInvalidCNPJ.CNPJ = '123456789'

        request.post(
            {url: urlBase, json: userWithInvalidCNPJ},
            function (error, response, body) {

                expect(response.statusCode).to.equal(400)

                if (body.should.have.property('errors')) {
                    expect(body.errors[0].stack).to.equal('instance.CNPJ does not meet minimum length of 14')
                }
                done()
            }
        )
    })

    it('Criar novo User com CEP inválido', function (done) {

        // Clona JSON original válido
        const userWithInvalidCEP = JSON.parse(JSON.stringify(userJson))
        // Altera para 'CEP' inválido
        userWithInvalidCEP.address.CEP = '123456'

        request.post(
            {url: urlBase, json: userWithInvalidCEP},
            function (error, response, body) {

                expect(response.statusCode).to.equal(400)

                if (body.should.have.property('errors')) {
                    expect(body.errors[0].stack).to.equal('instance.address.CEP does not meet minimum length of 8')
                }
                done()
            }
        )
    })
})

describe(`Criar novo User PF SEM Sucesso - POST ${urlBase}`, function () {

    // JSON válido utilizado como base para os cenários de falha
    const userJson = {
        "name": {
            "first": "Clark",
            "last": "Kent"
        },
        "typePerson": "PESSOA_FISICA",
        "CPF": "12345678901",
        "birth": "01/01/1900",
        "address": {
            "CEP": "96600123",
            "street": "Av principal",
            "number": 1212,
            "city": "Metropoli",
            "state": "RS"
        }
    }

    it('Criar novo User com campo "birth" inválido', function (done) {

        // Clona JSON original válido
        const userWithInvalidBirth = JSON.parse(JSON.stringify(userJson))
        // Altera para 'birth' inválido
        userWithInvalidBirth.birth = '1990/10/10'

        request.post(
            {url: urlBase, json: userWithInvalidBirth},
            function (error, response, body) {

                expect(response.statusCode).to.equal(400)

                if (body.should.have.property('errors')) {
                    expect(body.errors[0].argument).to.equal('[0-9]{2}/[0-9]{2}/[0-9]{4}')
                }
                done()
            }
        )
    })

    it('Criar novo User com campo "typePerson" inválido', function (done) {

        // Clona JSON original válido
        const userWithInvalidTypePerson = JSON.parse(JSON.stringify(userJson))
        // Altera para 'typePerson' inválido
        userWithInvalidTypePerson.typePerson = 'PF'

        request.post(
            {url: urlBase, json: userWithInvalidTypePerson},
            function (error, response, body) {

                expect(response.statusCode).to.equal(400)

                if (body.should.have.property('errors')) {
                    expect(body.errors[0].argument[0]).to.equal('PESSOA_JURIDICA')
                    expect(body.errors[0].argument[1]).to.equal('PESSOA_FISICA')
                }
                done()
            }
        )
    })
})


function jsonParser(jsonBody) {
    // precisa converter o retorno para um objeto JSON
    let body = {}
    try {
        //console.log(jsonBody)
        body = JSON.parse(jsonBody)
    } catch (err) {
        console.log(`Erro ao converter JSON Body! ${err}`)
        body = {}
    }
    return body
}
