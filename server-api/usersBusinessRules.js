const axios = require('axios')
const Validator = require('jsonschema').Validator

const typePersonPJ = 'PESSOA_JURIDICA'
const typePersonPF = 'PESSOA_FISICA'

// JSON Schema base para permitir criar um User válido
const userSchema = {
    "id": "/user",
    "type": "object",
    "properties": {
        "name": {"$ref": "/userName"},
        "typePerson": {
            "enum": [
                `${typePersonPJ}`,
                `${typePersonPF}`
            ]
        },
        "CPF": {
            "type": "string",
            "minLength": 11,
            "maxLength": 11
        },
        "CNPJ": {
            "type": "string",
            "minLength": 14,
            "maxLength": 14
        },
        "birth": {
            "type": "string",
            "pattern": "[0-9]{2}/[0-9]{2}/[0-9]{4}"
        },
        "address": {"$ref": "/userAddress"}
    },
    "required": ["name", "typePerson", "birth", "address"],
    "anyOf": [
        {
            "properties": {
                "typePerson": {"const": `${typePersonPJ}`}
            },
            "required": ["CNPJ"]
        },
        {
            "properties": {
                "typePerson": {"const": `${typePersonPF}`}
            },
            "required": ["CPF"]
        }
    ]
}

// JSON Schema base do nome do User
const userNameSchema = {
    "id": "/userName",
    "type": "object",
    "properties": {
        "first": {
            "type": "string",
            "minLength": 3
        },
        "last": {
            "type": "string",
            "minLength": 3
        }
    },
    "required": ["first", "last"],
    "additionalProperties": false
}

// JSON Schema base do Endereço do User
const userAddressSchema = {
    "id": "/userAddress",
    "type": "object",
    "properties": {
        "CEP": {
            "type": "string",
            "minLength": 8,
            "maxLength": 8
        },
        "street": {"type": "string"},
        "number": {
            "type": "integer",
            "minimum": 1
        },
        "city": {"type": "string"},
        "state": {
            "type": "string",
            "minLength": 2,
            "maxLength": 3
        }
    },
    "required": ["CEP", "street", "number", "city", "state"],
    "additionalProperties": false
}

module.exports = (req, res, next) => {

    // criar ou editar usuário
    if (req.url.startsWith('/users') && (req.method === 'POST' || req.method === 'PUT')) {

        let v = new Validator()

        // adiciona validação dos Schemas para Nome e Endereço
        v.addSchema(userNameSchema, '/userName')
        v.addSchema(userAddressSchema, '/addressSchema')

        // valida o schema do JSON
        let validatorResult = v.validate(req.body, userSchema)

        // se não possuir nenhum erro de validação
        if (validatorResult.errors.length === 0) {

            // monte URL com parâmetro de filtro por CPF ou CNPJ
            let urlWithFilterParam = req.headers.host + req.url
            if (req.body.typePerson === 'PESSOA_FISICA') {
                urlWithFilterParam += `?CPF=${req.body.CPF}`
            } else {
                urlWithFilterParam += `?CNPJ=${req.body.CNPJ}`
            }

            // consulta API users pelo CPF ou CNPJ
            // axios.delete(`${urlWithFilterParam}`)
            //     .then(resp => {
            //         console.log('fez GET!')
            //         console.log(resp.data)
            //     }).catch(error => {
            //     console.log(error)
            // })

            // valida CPF ou CNPJ que deve ser único
            next()

            // if (user === undefined) {
            //     // User NÃO existe
            //     next()
            // } else {
            //     console.log('User já existe!')
            //     // não cria o User e retorna os erros de validação
            //     res.status(400).jsonp({
            //         message: 'User já existe com o CPF/CNPJ informado!'
            //     })
            // }
        } else {
            console.log(validatorResult.errors)

            // não cria o User e retorna os erros de validação
            res.status(400).jsonp({
                message: 'Erro ao validar dados informados!',
                errors: validatorResult.errors
            })
        }
    } else {
        next()
    }
}
