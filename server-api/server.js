const jsonServer = require('json-server')
const usersBusinessRules = require('./usersBusinessRules.js')

const dbFile = './db.json'
const port = 8999

const server = jsonServer.create()
const router = jsonServer.router(dbFile)

server.use(jsonServer.defaults())
server.use(jsonServer.bodyParser)
server.use(usersBusinessRules)
server.use(router)

server.listen(port, () => {
    console.log(`JSON Server is running in port ${port} ...`)
})
